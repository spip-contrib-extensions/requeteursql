<?php
/**
 * Options du plugin Requêteur SQLau chargement
 *
 * @plugin     Requêteur SQL
 * @copyright  2014
 * @author     David Dorchies
 * @licence    GNU/GPL
 * @package    SPIP\Requeteursql\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) { return; }
