<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/requeteursql-requeteursql?lang_cible=gl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'export_classique' => 'CSV clásico (,)',
	'export_excel' => 'CSV para Excel (;)',
	'export_format' => 'Formato do ficheiro:',
	'export_tabulation' => 'CSV con tabulación',
	'exporter' => 'Exportar'
);
