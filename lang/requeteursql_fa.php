<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/requeteursql-requeteursql?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'export_classique' => 'سي.اس.وي استاندارد (،)',
	'export_excel' => 'سي.وي.اس براي اكسل (؛)',
	'export_format' => 'فرمت پرونده :‌',
	'export_tabulation' => 'سي.اس.وي با تب‌ جداساز',
	'exporter' => 'صادر سازي '
);
