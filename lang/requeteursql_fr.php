<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/requeteursql.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'export_classique' => 'CSV classique (,)',
	'export_csv' => 'Exporter au format CSV',
	'export_excel' => 'CSV pour Excel ( ;)',
	'export_format' => 'Format du fichier :',
	'export_sql' => 'Exporter une requête SQL',
	'export_tabulation' => 'CSV avec tabulations',
	'exporter' => 'Exporter',

	// R
	'requeteursql_titre' => 'Requêteur SQL'
);
