<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/requeteursql-requeteursql?lang_cible=ca
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'export_classique' => 'CSV clàssic (,)',
	'export_excel' => 'CSV per Excel (;)',
	'export_format' => 'Format de l’arxiu:',
	'export_tabulation' => 'CSV amb tabulacions',
	'exporter' => 'Exportar'
);
