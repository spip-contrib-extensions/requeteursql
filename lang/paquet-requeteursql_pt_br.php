<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/requeteursql-paquet-xml-requeteursql?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'requeteursql_description' => 'Esta ferramenta permite:
- Gerenciar uma lista de requisições SQL
- Exportar os resultados das requisições em formato CSV

Este plugin requer a instalação do plugin [Coloration code->https://plugins.spip.net/coloration_code.html] para a exibição da requisição SQL com a colorização sintática.

O acesso à lista de requisições é feito pelo menu Edição.

Por questões de segurança, apenas o webmaster tem a possibilidade de criar e alterar as requisições e apenas os administradores têm a possibilidade de as visualizar e executar.',
	'requeteursql_nom' => 'Requerente SQL',
	'requeteursql_slogan' => 'Para memorizar e executar as requisições SQL'
);
