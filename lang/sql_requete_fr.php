<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/requeteursql.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_sql_requete' => 'Ajouter cette requête',

	// C
	'choisir_table' => 'Choisir une table',

	// I
	'icone_creer_sql_requete' => 'Créer une requête',
	'icone_modifier_sql_requete' => 'Modifier cette requête',
	'info_1_sql_requete' => 'Une requête',
	'info_aucun_sql_requete' => 'Aucune requête',
	'info_nb_sql_requetes' => '@nb@ requêtes',
	'info_sql_requetes_auteur' => 'Les requêtes de cet auteur',

	// L
	'label_confirmation_danger' => 'Je prends quand même le risque',
	'label_description' => 'Description de la requête',
	'label_requetesql' => 'Requête SQL',
	'label_titre' => 'Titre de la requête',

	// M
	'message_erreur_requete_dangereuse' => 'La requête contient le mot « @mot@ », et est donc susceptible de modifier ou d’effacer des données dans la base. Vous ne devriez pas continuer à moins d’être absolument certain(e) de ce que vous faites !!',

	// N
	'not_exist' => 'Cette requête n’existe pas',

	// P
	'previsu' => 'Prévisualisation des 100 premiers enregistrements de la requête',

	// R
	'retirer_lien_sql_requete' => 'Retirer cette requête',
	'retirer_tous_liens_sql_requetes' => 'Retirer toutes les requêtes',

	// T
	'tables_champs' => 'Rechercher les tables et leurs champs',
	'texte_ajouter_sql_requete' => 'Ajouter une requête',
	'texte_changer_statut_sql_requete' => 'Cette requête est :',
	'texte_creer_associer_sql_requete' => 'Créer et associer une requête',
	'titre_langue_sql_requete' => 'Langue de cette requête',
	'titre_logo_sql_requete' => 'Logo de cette requête',
	'titre_sql_requete' => 'Requête SQL',
	'titre_sql_requetes' => 'Requêtes SQL',
	'titre_sql_requetes_rubrique' => 'Requêtes de la rubrique'
);
