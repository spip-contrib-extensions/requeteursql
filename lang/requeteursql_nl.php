<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/requeteursql-requeteursql?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'export_classique' => 'Klassieke CSV (,)',
	'export_excel' => 'CSV voor Excel ( ;)',
	'export_format' => 'Bestandsformaat:',
	'export_tabulation' => 'CSV met tabs',
	'exporter' => 'Exporteren'
);
