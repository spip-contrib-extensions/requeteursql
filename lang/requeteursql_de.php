<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/requeteursql-requeteursql?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'export_classique' => 'Klassisches CSV (,)',
	'export_excel' => 'CSV für Excel (;)',
	'export_format' => 'Format der Datei:',
	'export_tabulation' => 'CSV mit Tabulatoren',
	'exporter' => 'Exportieren'
);
