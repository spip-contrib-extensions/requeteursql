<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/requeteursql-requeteursql?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'export_classique' => 'CSV clássico (,)',
	'export_csv' => 'Exportar em formato CSV',
	'export_excel' => 'CSV para Excel ( ;)',
	'export_format' => 'Formato do arquivo:',
	'export_sql' => 'Exportar uma requisição SQL',
	'export_tabulation' => 'CSV com tabulação',
	'exporter' => 'Exportar',

	// R
	'requeteursql_titre' => 'Requerente SQL'
);
