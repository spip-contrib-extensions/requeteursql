<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/requeteursql-requeteursql?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'export_classique' => 'стандартный CSV (,)',
	'export_excel' => 'CSV для Excel ( ;)',
	'export_format' => 'Формат файла:',
	'export_tabulation' => 'CSV с разделителями табуляцией'
);
