<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/requeteursql-requeteursql?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'export_classique' => 'štadardný CSV (,)',
	'export_excel' => 'CSV Excelu (;)',
	'export_format' => 'Formát súboru:',
	'export_tabulation' => 'CSV oddelený tabulátorom',
	'exporter' => 'Exportovať'
);
