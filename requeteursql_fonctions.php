<?php
/**
 * Fonctions utiles au plugin Requêteur SQL
 *
 * @plugin     Requêteur SQL
 * @copyright  2014
 * @author     David Dorchies
 * @licence    GNU/GPL
 * @package    SPIP\Requeteursql\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) { return; }
